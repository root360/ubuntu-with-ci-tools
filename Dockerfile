# Ubuntu with tools for CI
FROM    ubuntu:jammy

ENV	DEBIAN_FRONTEND noninteractive

ADD     --chown=_apt:root https://download.docker.com/linux/ubuntu/gpg /etc/apt/trusted.gpg.d/docker-ce.asc

RUN	apt-get update \
	&& apt-get install -y \
		apt-transport-https \
		autoconf \
		automake \
		awscli \
		build-essential \
		curl \
		expect \
		git \
		jq \
		libffi-dev \
		libssl-dev \
		openssh-client \
		vim \
		python3 \
		python3-setuptools \
		python3-pip \
		python3-dev \
	&& echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu jammy stable" > /etc/apt/sources.list.d/docker-ce.list \
	&& apt-get update \
	&& apt-get install -y \
		docker-ce-cli \
	&& cd /tmp \
	&& curl -sSL "$(curl -sSL "https://api.github.com/repos/koalaman/shellcheck/releases" | grep browser_download_url | grep -F '.tar' | grep "linux.x86_64" | head -1 | cut -d'"' -f4)" | tar xJ \
	&& mv shellcheck-*/shellcheck /usr/bin/shellcheck \
	&& apt-get clean \
	&& apt-get -y autoremove \
	&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
